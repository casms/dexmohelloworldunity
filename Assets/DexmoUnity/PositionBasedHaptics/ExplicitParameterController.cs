﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UCL.CASMS.Haptics
{
    public class ExplicitParameterController : MonoBehaviour
    {
        public DexmoHandController hand;

        public string Parameter;
        public float Stiffness;
        public float Position;
        public bool Direction;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            hand.SetHaptics(hand.GetParameter(Parameter), true, Position, Direction, Stiffness);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UCL.CASMS.Haptics
{
    /// <summary>
    /// Computes haptic feedback paramters by resolving collisions between the world and a fingertip proxy, and finding 
    /// the closest point on the potential trajectory to the resolved proxy.
    /// </summary>
    public class GodObjectParameterController : MonoBehaviour
    {
        private new Collider collider;
        public HapticParameterCurve Parameter;

        private List<Collider> currentColliders = new List<Collider>();

        public float scale = 1.0f;

        private void Awake()
        {
            Parameter = GetComponentInParent<HapticParameterCurve>();
            collider = GetComponent<Collider>();
        }

        // Use this for initialization
        void Start()
        {
            currentColliders.Clear();
        }

        public void Update()
        {
            float finalparam = 1f;

            foreach (var other in currentColliders)
            {
                Vector3 direction = Vector3.zero;
                float distance = 0f;

                // find the offset of the finger trajectory endpoint necessary to resolve the collision of its geometry

                if (Physics.ComputePenetration(
                    collider, transform.position, transform.rotation,
                    other, other.transform.position, other.transform.rotation,
                    out direction, out distance
                    ))
                {
                    //Debug.DrawLine(transform.position, transform.position + direction * distance);

                    // get the position on the trajectory

                    // target position in curve space
                    var target = Parameter.transform.InverseTransformPoint(transform.position + direction * distance * scale);
                    var closest = Parameter.Curve.GetClosestPoint(target);

                    if (closest.element < finalparam)
                    {
                        finalparam = closest.element;
                    }
                }
            }

            var param = Parameter.HandController.GetParameter(Parameter.Parameter);

            if (finalparam < 1f)
            {
                Parameter.HandController.SetHaptics(param, true, finalparam, true, 5.0f);
            }
            else
            {
                Parameter.HandController.SetHaptics(param, true, 0, true, 0.0f);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            currentColliders.Add(other);
        }

        private void OnTriggerExit(Collider other)
        {
            currentColliders.Remove(other);
        }
    }
}
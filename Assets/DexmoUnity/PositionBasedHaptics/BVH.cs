﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UCL.CASMS.Haptics
{
    [Serializable]
    public class BVH
    {
        public struct Distance
        {
            public float min;
            public float max;

            public Distance(float min, float max)
            {
                this.min = min;
                this.max = max;
            }
        }

        [Serializable]
        public class Node
        {
            public Node(Node one, Node two)
            {
                index = -1;

                this.one = one.index;
                this.two = two.index;

                var norm = (one.position - two.position).normalized;
                var extreme1 = one.position + norm * one.radius;
                var extreme2 = two.position - norm * two.radius;

                position = (extreme1 + extreme2) / 2f;
                radius = (extreme1 - position).magnitude;

                isLeaf = false;
            }

            public Node(Vector3 position, float radius, int item)
            {
                index = -1;

                this.position = position;
                this.radius = radius;
                this.item = item;

                isLeaf = true;
            }

            public int index;

            public Vector3 position;
            public float radius;

            public int one;
            public int two;

            public int item;

            public bool isLeaf;

            public Distance GetDistances(Vector3 point)
            {
                var d = (point - position).magnitude;
                var min = d - radius;
                var max = d + radius;
                return new Distance(min, max);
            }
        }


        public bool initialised
        {
            get { return Nodes.Count > 0; }
        }

        public Node Root;
        public List<Node> Nodes;    // the tree must be stored flat as a set of structs, because unity only serialises structs (or classes that behave like structs)

        private class ClosestPointJob
        {
            public Vector3 point;
            public List<Node> nodes = new List<Node>();
            public List<Node> leaves = new List<Node>();
            public float smallestMaxDistance = float.PositiveInfinity;

            public void Clear()
            {
                nodes.Clear();
                leaves.Clear();
                smallestMaxDistance = float.PositiveInfinity;
            }
        }

        private ClosestPointJob Job;

        public virtual void Initialise(IEnumerable<Node> leaves)
        {
            // create the bvh. for this simple application we are assuming the primitives are well tesselated, so just pair them off with their neighbours

            Nodes = new List<Node>();

            var layer = leaves;
            while(layer.Count() > 1)
            {
                layer = NextLayer(layer);
            }
            if (layer.Count() == 1)
            {
                Root = layer.First();
            }
            if(layer.Count() <= 0)
            {
                throw new System.Exception();
            }
        }

        private Node IndexNode(Node node)
        {
            if(node.index < 0)
            {
                node.index = Nodes.Count;
                Nodes.Add(node);
            }
            return node;
        }

        private IEnumerable<Node> NextLayer(IEnumerable<Node> previouslayer)
        {
            var enumerator = previouslayer.GetEnumerator();

            while (enumerator.MoveNext())
            {
                var one = IndexNode(enumerator.Current);

                if (enumerator.MoveNext())
                {
                    var two = IndexNode(enumerator.Current);

                    // we have at least two nodes. combine them into one.
                    one = IndexNode(new Node(one, two));
                }

                // either this is a new node, or its the only remaining node from the last layer
                yield return one;
            }
        }

        /// <summary>
        /// Finds the closest leaf node(s) to the point in local (bvh) space. Multiple points will be returned
        /// if it is not obvious which contains the closest primitve.
        /// </summary>
        /// <param name="point"></param>
        public IEnumerable<int> ClosestPrimitive(Vector3 point)
        {
            if (Job == null)
            {
                Job = new ClosestPointJob();
            }

            Job.Clear();

            Job.point = point;
            Job.nodes.Add(Root);

            do
            {
                var job = Job.nodes[0];
                Job.nodes.RemoveAt(0);
                ProcessClosestNodeJob(job);
            } while (Job.nodes.Count > 0);

            foreach(var item in Job.leaves)
            {
                yield return item.item;
            }
        }

        private void ProcessClosestNodeJob(Node node)
        {
            // if this is a leaf node we are done. add it to the results and the caller will take over.
            if(node.isLeaf)
            {
                Job.leaves.Add(node);
                return;
            }

            // identify which children to add to the list of nodes to traverse. this is basically a set
            // of tests that try to cull nodes using implicit separating axis theorem tests.
            // (i.e. finding nodes that cannot be closer than those already encountered..)

            var distanceone = One(node).GetDistances(Job.point);
            var distancetwo = Two(node).GetDistances(Job.point);

            // update the minimum-max distance. any nodes with a minimum distance larger than this can't
            // contain the closest primitive, because some primitive has a maximum distance closer than 
            // them.

            Job.smallestMaxDistance = Mathf.Min(Job.smallestMaxDistance, distanceone.max);
            Job.smallestMaxDistance = Mathf.Min(Job.smallestMaxDistance, distancetwo.max);

            bool cullone = false;
            bool culltwo = false;

            if(distanceone.min > Job.smallestMaxDistance)
            {
                cullone = true;
            }
            if(distancetwo.min > Job.smallestMaxDistance)
            {
                culltwo = true;
            }

            if(distanceone.max < distancetwo.min)
            {
                // seperating axis between one and two
                culltwo = true;
            }

            if(distancetwo.max < distanceone.min)
            {
                cullone = true;
            }

            if (!cullone)
            {
                Job.nodes.Add(One(node));
            }
            if (!culltwo)
            {
                Job.nodes.Add(Two(node));
            }
        }

        private Node One(Node node)
        {
            return Nodes[node.one];
        }

        private Node Two(Node node)
        {
            return Nodes[node.two];
        }

        public void DebugDraw(Transform space)
        {
            DebugDrawTreelet(Root, space);
        }

        private void DebugDrawTreelet(Node root, Transform space)
        {
            DebugDrawNode(root, space);
            if (!root.isLeaf)
            {
                DebugDrawTreelet(Nodes[root.one], space);
                DebugDrawTreelet(Nodes[root.two], space);
            }
        }

        private void DebugDrawNode(Node node, Transform space)
        {
            var color = node.isLeaf ? Color.green : Color.blue;
            var v0 = space.TransformPoint(node.position);
            DebugCircle(v0, space.right, color, node.radius);
        }

        //Copyright (c) Arkham Interactive
        //Date : September 17th, 2013
        // ---------------------------------------------------------------------
        private static void DebugCircle(Vector3 position, Vector3 up, Color color, float radius = 1.0f, float duration = 0, bool depthTest = true)
        {
            Vector3 _up = up.normalized * radius;
            Vector3 _forward = Vector3.Slerp(_up, -_up, 0.5f);
            Vector3 _right = Vector3.Cross(_up, _forward).normalized * radius;

            Matrix4x4 matrix = new Matrix4x4();

            matrix[0] = _right.x;
            matrix[1] = _right.y;
            matrix[2] = _right.z;

            matrix[4] = _up.x;
            matrix[5] = _up.y;
            matrix[6] = _up.z;

            matrix[8] = _forward.x;
            matrix[9] = _forward.y;
            matrix[10] = _forward.z;

            Vector3 _lastPoint = position + matrix.MultiplyPoint3x4(new Vector3(Mathf.Cos(0), 0, Mathf.Sin(0)));
            Vector3 _nextPoint = Vector3.zero;

            color = (color == default(Color)) ? Color.white : color;

            for (var i = 0; i < 91; i++)
            {
                _nextPoint.x = Mathf.Cos((i * 4) * Mathf.Deg2Rad);
                _nextPoint.z = Mathf.Sin((i * 4) * Mathf.Deg2Rad);
                _nextPoint.y = 0;

                _nextPoint = position + matrix.MultiplyPoint3x4(_nextPoint);

                Debug.DrawLine(_lastPoint, _nextPoint, color, duration, depthTest);
                _lastPoint = _nextPoint;
            }
        }
        // ---------------------------------------------------------------------
    }
}
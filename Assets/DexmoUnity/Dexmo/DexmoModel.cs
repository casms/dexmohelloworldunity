﻿using System;
using System.Collections.Generic;
using UCL.CASMS.DH;

namespace UCL.CASMS.Haptics
{
    /// <summary>
    /// Builds a DH Model of the Dexmo Left Hand Glove
    /// </summary>
    public static class DexmoModel
    {
        public static Model Create()
        {
            var model = new Model("Dexmo Hand");

            model.Chains = new List<Chain>();

            float fh = -0.0033f;

            model.Chains.Add(CreateChain("index",   CreateFingerJoints(0.0427f, 0.003f, fh, 0.065f)));
            model.Chains.Add(CreateChain("middle",  CreateFingerJoints(0.0485f, 0.023f, fh, 0.065f)));
            model.Chains.Add(CreateChain("ring",    CreateFingerJoints(0.0534f, 0.044f, fh, 0.065f)));
            model.Chains.Add(CreateChain("little",  CreateFingerJoints(0.0484f, 0.066f, fh, 0.055f)));

            return model;
        }

        private static Chain CreateChain(string name, IEnumerable<Joint> joints)
        {
            var chain = new Chain(name);
            chain.Joints = new List<Joint>(joints);
            return chain;
        }

        /// <summary>
        /// Creates a DH Joint/Link chain of a Dexmo finger. The first joint is a fixed projection from the root,
        /// then the motor driver, and the per-finger segment.
        /// </summary>
        /// <param name="x">Units forward of the lower left screw</param>
        /// <param name="y">Units right of the lower left screw </param>
        /// <param name="z">Units up rom the case midline join</param>
        /// <param name="s">Final segment length</param>
        /// <returns></returns>
        private static IEnumerable<Joint> CreateFingerJoints(float x, float y, float z, float s)
        {
            Joint projection = DH.Parameter.MakeJoint("Projection", x, y, z);

            Joint motor = DH.Parameter.MakeJoint("Motor", 0.0196f, 0f, -0.0017f);   // the driver parameters are all the same
            motor.th = -projection.th;                                  // undo the direction of the projection so the motor points straight ahead
            motor.a = 90;                                               // roll counter clockwise and make the y axis face left, so positive angle results in abduction, which is the same polarity as the encoder values

            Joint arm = DH.Parameter.MakeJoint("Arm", 0.0574f, 0f, 0f); // the first segment of the arm is part of the standard motor and these parameters are the same
            arm.RelativeLimits(76, 57);

            Joint finger = DH.Parameter.MakeJoint("Finger", s, 0f, 0f); // the second segment is per-finger

            Joint cup = DH.Parameter.MakeJoint("Cup", 0.0057f, 0f, 0f); // the finger cups are again identical

            yield return projection;
            yield return motor;
            yield return arm;
            yield return finger;
            yield return cup;
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UCL.CASMS.DH
{
    /// <summary>
    /// Part of a DH segment. A chain of DHNode components implements a chain of DH links, updating the Unity transforms
    /// according to their parameters.
    /// In the DH convention, there is a rotation in the local reference frame, a translation, then a rotation in the
    /// transformed reference frame, so a traditional transform matrix is unable to represent an entire link.
    /// Each DHNode maintains an endpoint. The endpoint can also be a DHNode, in which case it will manage itself
    /// with knowledge of its parent's parameters.
    /// </summary>
    [ExecuteInEditMode]
    public class DHNode : MonoBehaviour
    {
        [SerializeField]
        public Joint joint;

        public bool SetModelFromTransform = false;

        public GameObject Endpoint
        {
            get
            {
                
                if (endpoint == null)
                {
                    foreach (Transform t in transform)
                    {
                        if (t.gameObject.GetComponent<DHNode>())
                        {
                            endpoint = t.gameObject;
                            break;
                        }
                    }
                }

                // Create the endpoint if we really can't find one
                if (endpoint == null)
                { 
                    endpoint = new GameObject("Endpoint");
                    endpoint.transform.parent = this.transform;
                }

                return endpoint;
            }
        }
        [SerializeField]
        private GameObject endpoint;

        public float Distance
        {
            get
            {
                return Mathf.Sqrt(joint.r * joint.r + joint.d * joint.d);
            }
        }

        // Update is called once per frame
        public void Update()
        {
            // update the name of this node (in case we are a child of another gameobject currently called endpoint)

            if (joint.Name != null)
            {
                name = joint.Name;
            }
            else
            {
                joint.Name = name;
            }

            if (SetModelFromTransform)
            {
                GetTransform();
            }
            else
            {
                SetTransform();
            }

            Debug.DrawLine(transform.position, Endpoint.transform.position);
            Debug.DrawLine(transform.position, transform.position + transform.right * 0.01f, Color.red);
            Debug.DrawLine(transform.position, transform.position + transform.up * 0.01f, Color.green);
            Debug.DrawLine(transform.position, transform.position + transform.forward * 0.01f, Color.blue);
        }

        /// <summary>
        /// Updates the transform of this node with the specified DH parameters
        /// </summary>
        void SetTransform()
        {
            var a = 0f;

            if (transform.parent != null)
            {
                var parentnode = transform.parent.gameObject.GetComponent<DHNode>();
                if (parentnode != null)
                {
                    a = parentnode.joint.a;
                }
            }

            // Update this node's rotation
            transform.localRotation = Quaternion.AngleAxis(a, Vector3.forward) * Quaternion.AngleAxis(joint.th, Vector3.up);

            // Update the endpoints position
            Endpoint.transform.localPosition = (Vector3.forward * joint.r) + (Vector3.up * joint.d);

            // Does the endpoint need its inclination adjusted, or will it do it itself?
            var remotenode = Endpoint.GetComponent<DHNode>();
            if (remotenode == null)
            {
                Endpoint.transform.localRotation = Quaternion.AngleAxis(joint.a, Vector3.forward);
            }
        }

        /// <summary>
        /// Updates the DH parameters of this node based on the actual transforms
        /// </summary>
        void GetTransform()
        {
            joint.th = transform.localRotation.eulerAngles.y;
            joint.a = Endpoint.transform.localRotation.eulerAngles.z;
            joint.r = Endpoint.transform.localPosition.z;
            joint.d = Endpoint.transform.localPosition.y;
        }

    }

}

# README #

Example project showing how to use the Dexmo in Unity. Contains the Dexmo SDK and the UCL CASMS DexmoUnity project.

Please be aware this project contains third-party assets purchased for this project only. These assets may not be re-distributed or used in any derivative works. Please contact us for more information on which assets are safe to use.